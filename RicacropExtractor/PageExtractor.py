import pandas
import openpyxl


class PageExtractor:

    def __init__(self, url: str):
        self.encoding = "utf-8"
        self.tbs = pandas.read_html(url, encoding=self.encoding)
        self.identify_building()
        self.identify_building_details()
        self.identify_floor_names()
        self.identify_unit_names()
        self.identify_unit_tables()

    def identify_building(self):
        """
        Identify the table containing estate and building information
        and extract them.
        """
        for tb in self.tbs:
            x = str(tb.iloc[0][0])
            # we use two key words to idenitfy that the table
            # contains estate and building information
            # if not, skip that table
            if not(len(x)) > 2 or not x[:2] == "主頁" or not " > " in x:
                continue
            # if yes, we extract the contents
            while "  >" in x:
                x = x.replace("  >", " >")
            while ">  " in x:
                x = x.replace(">  ", "> ")
            x = x.split(" > ")
            self.building = ""
            for i in range(1, len(x)):
                y = x[i]
                self.building += y.split("  ")[0] + "_"
            self.building = self.building[:-1]
            break

    def identify_building_details(self):
        """
        Identify the table containing building detail information (e.g. address, num. of floor...)
        and extract them.
        """
        for tb in self.tbs:
            x = tb.iloc[0][0]
            # we use key word "地址:" to identify the table
            # containing the information we need
            # if not, skip that table
            if not x == "地址:":
                continue
            # if yes, we extract the contents
            for i in range(len(tb.index)):
                x = tb.iloc[i][0]
                y = tb.iloc[i][1]
                if "地址" in x:
                    self.address = y
                elif "入伙日期" in x:
                    self.occupationDate = y
                elif "單位數目" in x:
                    self.numOfUnits = y
                elif "層數" in x:
                    self.numOfFloors = y
                elif "每層伙數" in x:
                    self.unitsPerFloor = y
                elif "所屬校網" in x:
                    self.schoolArea = y
                elif "發展商" in x:
                    self.developer = y
                elif "管理公司" in x:
                    self.managementCompany = y
                elif "物業設施" in x:
                    self.facilities = y
            break

    def identify_floor_names(self):
        """
        Identify floor names of the building.
        """
        for tb in self.tbs:
            x = tb.iloc[0][0]
            # we use key word "樓/室" to identify the table
            # if not, skip that table
            if not x == "樓/室":
                continue
            # if yes, we extract the floors
            self.floors = []
            for i in range(1, len(tb.index)):
                self.floors.append(tb.iloc[i][0])
            break

    def identify_unit_names(self):
        """
        Identify unit names of each floor.
        """
        keywords = "實用(呎)  建築(呎)  價錢  年份  利潤"
        for tb in self.tbs:
            # some table only contains one row
            # which is not our target table
            if len(tb.index) < 2:
                continue
            x = str(tb.iloc[1][0])
            # we use a serious of key words to identify the table
            # if not, skip that table
            if not len(x) >= len(keywords) or not x[:len(keywords)] == keywords:
                continue
            # if yes, extract the unit names
            self.units = []
            for i in range(len(tb.columns)):
                self.units.append(tb.iloc[0][i])
            break

    def identify_unit_tables(self):
        """
        Identify unit tables.
        """
        self.unitTables = []
        for tb in self.tbs:
            cols = list(tb.columns)
            # we use the items of column to identify the table
            # if not, skip that table
            if not cols == ["實用(呎)", "建築(呎)", "價錢", "年份", "利潤"]:
                continue
            # if yes, append the table to list
            self.unitTables.append(tb)

    def export_to_excel(self):
        self.wb = openpyxl.Workbook()
        self.__create_buildingInfo_sheet()
        self.__create_unitInfo_sheet()
        self.wb.save(self.building+".xlsx")

    def __create_buildingInfo_sheet(self):
        sh = self.wb.create_sheet("building_info", 0)
        titles = [
            "樓宇",
            "地址",
            "入伙日期",
            "單位數目",
            "層數",
            "每層伙數",
            "所屬校網",
            "發展商",
            "管理公司",
            "物業設施"
        ]
        for i in range(1, 11):
            sh.cell(i, 1).value = titles[i-1]
        contents = [
            self.building,
            self.address,
            self.occupationDate,
            self.numOfUnits,
            self.numOfFloors,
            self.unitsPerFloor,
            self.schoolArea,
            self.developer,
            self.managementCompany,
            self.facilities
        ]
        for i in range(1, 11):
            sh.cell(i, 2).value = contents[i-1]

    def __create_unitInfo_sheet(self):
        sh = self.wb.create_sheet("units", 1)
        headers = [
            "樓層",
            "單位",
            "實用呎",
            "建築呎",
            "價錢",
            "年份",
            "利潤"
        ]
        for i in range(1, 8):
            sh.cell(1, i).value = headers[i-1]
        row = 2
        for tbOrder in range(len(self.unitTables)):
            tb = self.unitTables[tbOrder]
            for tbRow in range(len(tb.index)):
                sh.cell(row, 1).value = self.floors[tbRow]
                sh.cell(row, 2).value = self.units[tbOrder]
                for i in range(len(tb.columns)):
                    sh.cell(row, i+3).value = tb.iloc[tbRow][i]
                row += 1
