from selenium import webdriver
from tkinter import messagebox
import chromedriver_autoinstaller

from .PageExtractor import PageExtractor

class AutoChrome:

    def __init__(self):
        driver = chromedriver_autoinstaller.install(cwd=True)
        self.chrome = webdriver.Chrome(driver)
        home_url = "https://www.ricacorp.com/Ricapih09/pasttranindex.aspx"
        self.chrome.get(home_url)
        self.url = home_url
        self.monitor_url()
    
    def monitor_url(self):
        while True:
            while (self.chrome.current_url == self.url):
                continue
            print("New page detected.")
            self.url = self.chrome.current_url
            print("try to extract page contents.")
            try:
                extractor = PageExtractor(self.url)
                extractor.export_to_excel()
                messagebox.showinfo("Save Page", "Saved as {}.xlsx".format(extractor.building))
            except:
                print("Failed.")